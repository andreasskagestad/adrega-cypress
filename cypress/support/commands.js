// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })


Cypress.Commands.add('login', (username, password) => {
	if(username === undefined){
		username = 'cypress';
		password = 'Cypress4u';
	}
	
	cy.visit('https://asp3.adrega.no/Cypress').then(($myElement) => {
		cy.get('#m_LoginDialog__txtUserName').type(username);
		cy.get('#m_LoginDialog__txtPassword').type(password);
		cy.get('#m_LoginDialog__btnLogon').click();
	});
});

Cypress.Commands.add('logout', () => {
	cy.get('right-menu', {timeout: 20000}).click();
	cy.get('div>span').last().parent().click({waitForAnimations: false});
});
