describe(
	'Adrega Timereporting Tests', 
	{
		viewportHeight: 1200,
		viewportWidth: 1600,
	},
	function () {

		it('Log In', function () {
			cy.login().then(($myElement) => {
				cy.get('main-menu>menu-item>div', {timeout: 20000}).contains('Time Reporting').click();
			});
		});
		
		it('Timesheet-Left Table: Show Level', function () {
		
			var levelDropdown = cy.get('mat-toolbar > kendo-dropdownlist').eq(0);

			 cy.get('tbody[kendotreelisttablebody] > tr').should('have.length', 1);
			
			levelDropdown.click();
			cy.get('kendo-popup').find('li').contains('Level 2').click().then(($myElement) => {
				 cy.get('tbody[kendotreelisttablebody] > tr').should('have.length', 5);
			});				
			levelDropdown.click();
			cy.get('kendo-popup').find('li').contains('All').click().then(($myElement) => {
				cy.get('tbody[kendotreelisttablebody] > tr').should('have.length', 6);
			});	
			levelDropdown.click();
			cy.get('kendo-popup').find('li').contains('Level 1').click().then(($myElement) => {
				cy.get('tbody[kendotreelisttablebody] > tr').should('have.length', 1);
			});
		});
		
		it('Timesheet-Left Table: Manual expand', function () {
			cy.get('tbody[kendotreelisttablebody] > tr > td').eq(0).click();
			cy.get('tbody[kendotreelisttablebody] > tr').should('have.length', 5);
		});
		
		it('Timesheet-Left Table: Add Timesheets', function () {			
			cy.get('tbody[kendotreelisttablebody] > tr').contains('DblClick').click().dblclick().then(($myElement) => {
				cy.get('tbody[kendotreelisttablebody] > tr').contains('ArrowInsert').click().then(($myElement) => {
					cy.get('time-reporting-search-results').find('button[title="Add item to timesheets"]').click();
				});				
			});
		});
		
		it('Timesheet-Left Table: Pin Items', function () {		
			cy.get('div').contains('Quick access (0)');
			cy.get('tbody[kendotreelisttablebody] > tr').contains('QuickAccess').click().then(($myElement) => {		
				cy.get('time-reporting-search-results').find('button[title="Pin item"]').click();
				cy.get('div').contains('Quick access (1)').click();	
			});		
		});
		
		it('Timesheet-Left Table: Add Timesheet from Quick Access', function () {		
				
		});
	}
);