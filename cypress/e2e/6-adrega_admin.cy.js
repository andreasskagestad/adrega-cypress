describe(
	'Adrega Admin Tests', 
	{
		viewportHeight: 1200,
		viewportWidth: 1600,
	},
	
	function () {
	
	it('Log In', function () {
		cy.login().then(($myElement) => {
			cy.get('main-menu>menu-item>div', {timeout: 20000}).contains('Administration').click();
		});
	});
	
	it('Admin Users: Create user.', function () {
		//Create a user of each type
		cy.get('img[src="images/icons8_add_user_24.png"]').click();
		cy.get('input[name^=userCode]').type('testmember');
		cy.get('input[name^=userName]').type('Cypress Test New Member');
		cy.get('input[formcontrolname^=userPassword]').type('test');
		cy.get('input[formcontrolname^=verifyPassword]').type('test');
		cy.get('admin-edit-users').find('button').eq(1).click();	
		cy.logout();
	});
	
	it('Admin User: Test that member is set up conrrectly', function () {
		cy.login('testmember','test').then(($myElement) => {
			cy.get('main-menu>menu-item', {timeout: 20000}).should('have.length', 3);
			cy.logout();
		});
	});
	
	it('Admin User: Disable User', function () {
		cy.login().then(($myElement) => {
			cy.get('main-menu>menu-item>div', {timeout: 20000}).contains('Administration').click();
			cy.get('#checkSelect46').click();
			cy.get('button[title^="Save changes"]').click();
			cy.logout();
			cy.login('testmember','test');
			cy.get('span').contains('permission to access');
		});
	});
	
	it('Admin User: Delete users', function () {
		cy.login().then(($myElement) => {
			cy.get('main-menu>menu-item>div', {timeout: 20000}).contains('Administration').click();
			cy.wait(1000);
			cy.get('tbody').contains('testmember').click();
			
			cy.get('img[src="images/icons8_delete_selected_rows_24px.png"]').click();
			cy.get('button').contains('Yes').click();
		});	
	});
	
	/*it('Log out. Dashboard Tests Complete', function () {
		cy.logout();
	});*/

});