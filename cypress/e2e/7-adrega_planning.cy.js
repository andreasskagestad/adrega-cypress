describe(
	'Adrega Planning Tests', 
	{
		viewportHeight: 1200,
		viewportWidth: 1600,
	},
	
	function () {
	
	it('Log In and load Projects Page', function () {
		cy.login().then(($myElement) => {
			cy.get('main-menu>menu-item>div', {timeout: 20000}).contains('Project').click();
		});
	});
	
	//Remove this test to trigger conditions described in Bug 13264: Web Projects: Columns dialog doesn't load correctly if main page data isn't loaded
	it('Projects: Check that the project list loads correctly (WORKAROUND 13264)', function () {
		cy.get('#project-list-table').find('kendo-grid-list').find('tbody > tr').should('have.length', 11);
	});
	
	//13264 should trigger if the test above is removed.
	it('Column Dialog: Test opening and data load in both tables.', function () {
		cy.get('#select-columns').click();
		cy.get('#fieldListView').find('tr[kendotreelistlogicalrow]').should('have.length', 3);
		cy.get('#columnTable').find('tr').should('have.length', 13);
	});
	
	it('Column Dialog: Show level (slow, should be improved)', function () {
		cy.get('#menu').click();
		cy.get('span').contains("Show level").click();
		cy.get('span').contains('2').click().wait(1000).then(($myElement) => {
			cy.get('#fieldListView').find('tr[kendotreelistlogicalrow]').should('have.length', 421);		
		});
		cy.get('#menu', {timeout: 20000}).click();
		cy.get('span').contains("Show level").click();
		cy.get('span', {timeout: 20000}).contains('5').click().wait(1000).then(($myElement) => {
			cy.get('#fieldListView').find('tr[kendotreelistlogicalrow]').should('have.length', 487);		
		});
	});
	
	it('Column Dialog: Test that filter works', function () {
		cy.get('#quickSearch').find('input').type('count').then(($myElement) => {
			cy.get('tr[kendotreelistlogicalrow]', {timeout: 20000}).should('have.length', 5);
		});		
	});
	
	
	/*it('Log out. Dashboard Tests Complete', function () {
		cy.logout();
	});*/

});