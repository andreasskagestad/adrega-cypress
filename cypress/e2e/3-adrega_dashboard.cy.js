﻿describe(
	'Adrega Dashboard Tests', 
	{
		viewportHeight: 1200,
		viewportWidth: 1600,
	},
	function () {

	/*beforeEach(function () {
		//Cypress.Cookies.preserveOnce('ASP.NET_SessionId', 'SecurityId', 'UserId', 'client_name', 'client_uid', 'locale', 'loggedin', 'logontype', 'name', 'password', 'user_authenticated', 'user_uid',
		//'user_uid_site', 'username', 'Database storage', 'Local storage');
		
		cy.viewport(1600, 1200);
	});*/
	
	it('Log In and open Dashboard', function () {
		//Check login (remember checked)
		cy.login().then(($myElement) => {
			cy.get('main-menu>menu-item>div', {timeout: 20000}).contains('Dashboard').click();
		});
	});
	
	it('Determine if switch size works', function () {
		cy.get('div[class*="dashboard-column"]').should('have.length', 4);
		cy.get('img[src="images/icons8_maximize_24px.png"]').eq(0).click();
		cy.get('div[class*="dashboard-column"]').should('not.exist');
		cy.get('img[src="images/icons8_minimize_24px.png"]').click();		
	});
	
	it('Check change rows', function () {
		cy.get('kendo-dropdownlist').eq(1).click();
		cy.get('kendo-popup').find('li').eq(2).click();
		cy.get('div[class*="dashboard-column"]').should('have.length', 6);
	});
	
	it('Check change columns', function () {
		cy.get('kendo-dropdownlist').eq(2).click();
		cy.get('kendo-popup').find('li').eq(2).click();		
		cy.get('div[class*="dashboard-column"]').should('have.length', 9);
	});
	
	it('Check Update report(s) button', function () {
		cy.get('img[src="images/icons8_refresh_data_24px.png"]').eq(0).click();
		cy.get('div[class*="dashboard-column"]').should('have.length', 4);
	});
	
	it('Check reduce labels', function () {
		cy.get('g>text').contains('Dec');
		cy.get('kendo-dropdownlist').eq(3).click();
		cy.get('kendo-popup').find('li').eq(7).click();
		cy.wait(500); //workaround to make labels generate before test is triggered to prevent false positive.
		cy.get('g>text').contains('Dec').should('not.exist');
	});
	
	it('Check increase labels', function () {
		cy.get('kendo-dropdownlist').eq(3).click();
		cy.get('kendo-popup').find('li').eq(0).click();
		cy.wait(500);//workaround to make labels generate before test is triggered to prevent false negative.
		cy.get('g>text').contains('Dec');
	});
	
	
	/*it('Log out. Dashboard Tests Complete', function () {
		cy.get('right-menu').click();
		cy.get('div>span').last().parent().click();
	});*/
});