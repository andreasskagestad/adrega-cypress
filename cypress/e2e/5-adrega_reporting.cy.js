describe(
	'Adrega Reporting Tests', 
	{
		viewportHeight: 1200,
		viewportWidth: 1600,
	},
	function () {
	beforeEach(function () {
		//Cypress.Cookies.preserveOnce('ASP.NET_SessionId', 'SecurityId', 'UserId', 'client_name', 'client_uid', 'locale', 'loggedin', 'logontype', 'name', 'password', 'user_authenticated', 'user_uid',
		//'user_uid_site', 'username', 'Database storage', 'Local storage');	
	});
	
	it('Log In', function () {
		cy.login().then(($myElement) => {
			cy.get('main-menu>menu-item>div', {timeout: 20000}).contains(/^ Reporting $/).click();
		});
	});
	
	it('Open New Report Wizard', function () {
		//cy.viewport(1600, 1200);
		//cy.get('main-menu>menu-item').eq(3).click().wait(500).then(($myElement) => {
		cy.get('secondary-menu>menu-item-2>').eq(1).click().wait(500).then(($myElement) => {
			cy.get('mat-toolbar>button').eq(3).click().wait(500).then(($myElement) => {
			cy.get('kendo-window').find('kendo-grid-list').find('tr').eq(0).find('td').eq(0).click().then(($myElement) => {					
				});
			});
		});
		//});
	});
	
	it('Report Wizard - Step1 Add Project', function () {
		cy.get('kendo-window').find('wizard-page-select-projects>div>div').find('button').eq(2).click().then(($myElement) => {
		});
	});
	
	it('Report Wizard - Step2', function () {
		cy.get('kendo-buttongroup>button').eq(2).click().then(($myElement) => {
			cy.get('wizard-page-select-templates').find('kendo-grid-list').eq(0).find('tbody').find('tr').eq(1).find('td').eq(1).click().then(($myElement) => {
				cy.get('kendo-window').find('wizard-page-select-templates>div>div').find('button').eq(2).click().then(($myElement) => {
				});
			});
		});
	});
	
	it('Report Wizard - Step3', function () {
		cy.get('kendo-buttongroup>button').eq(2).click().then(($myElement) => {
			cy.get('wizard-page-bundle-details').find('input').eq(0).type('Cypress_Test_Bundle').then(($myElement) => {
				cy.get('kendo-buttongroup>button').eq(2).click();
			});
		});
	});
	
	it('Delete report', function () {
		cy.get('tbody').contains('Cypress_Test_Bundle').click().then(($myElement) => {
			cy.get('button[title^=Delete]').click().then(($myElement) => {
				cy.get('.btn-confirm').click();
			});
		});
	});
	
	/*it('Log out. Dashboard Tests Complete', function () {
		cy.logout();
	});*/
	
});