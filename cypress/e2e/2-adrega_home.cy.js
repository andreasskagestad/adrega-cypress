describe(
	'Adrega Home Tests', 
	{
		viewportHeight: 1200,
		viewportWidth: 1600,
	},
	
	function () {
	
		it('Log In and check that components are loaded', function () {
			cy.login().then(($myElement) => {
				cy.get('kendo-grid', {timeout: 20000}).should('have.length', 2);
			});
		});
		
		it('Check data load for My Timesheet Status', function () {
			//cy.get('kendo-grid').eq(0).find('tr').should('have.length', 18);	
		});
		
		it('Check data load for My Invoice Index', function () {
			cy.get('kendo-grid').eq(1).find('tr').should('have.length', 19); //varies between 18 and 19?	
		});
	
	}
);