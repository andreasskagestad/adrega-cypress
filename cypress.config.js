const { defineConfig } = require('cypress')

module.exports = defineConfig({
  projectId: 'u5w3hy',
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
	excludeSpecPattern: "./cypress/e2e/0-adrega_runall.cy.js",
	video: false
  },
})
